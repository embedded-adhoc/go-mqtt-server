package db

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"time"

	"adhoc.com/mqtt-server/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var once sync.Once

// type global

var (
	instance *mongo.Database
)

// DevMsg :	Device Date Set
type DevMsg = model.DevMsg

func MongoDB() *mongo.Database {

	once.Do(func() { // <-- atomic, does not allow repeating
		mongoURL := os.Getenv("DATABASE_URL")
		if len(mongoURL) == 0 {
			mongoURL = "mongodb://localhost:27017"
		}
		mongoClient, mongoErr := mongo.NewClient(options.Client().ApplyURI(mongoURL))
		if mongoErr != nil {
			fmt.Print("MONGO ERRO:")
			fmt.Println(mongoErr)
		}
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		mongoErr = mongoClient.Connect(ctx)
		if mongoErr != nil {
			fmt.Print("MONGO ERRO:")
			fmt.Println(mongoErr)
		}
		mongoErr = mongoClient.Ping(ctx, readpref.Primary())
		if mongoErr != nil {
			fmt.Print("MONGO ERRO:")
			fmt.Println(mongoErr)
		}
		instance = mongoClient.Database("Device-Logging") // <-- thread safe

	})

	return instance
}

func Insert(collectionName string, obj DevMsg) (res *mongo.InsertOneResult, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	res, err = MongoDB().Collection(collectionName).InsertOne(ctx, obj)
	return res, err
}

func GetDataByName(collectionName string, name string) (err error) {
	var ss bson.M
	collection := MongoDB().Collection(collectionName)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err = collection.FindOne(ctx, bson.M{}).Decode(&ss); err != nil {
		log.Fatal(err)
	} else {
		fmt.Println(ss, ss["id"], ss["data"])
	}
	return err
}

func GetAllCollection(collectionName string) (results []DevMsg, err error) {
	findOptions := options.Find()
	maxResult := string(os.Getenv("MAX_RESULT"))
	if len(maxResult) == 0 {
		maxResult = "100"
	}
	maxResultNum, _ := strconv.ParseInt(maxResult, 10, 64)
	findOptions.SetLimit(maxResultNum)
	findOptions.SetSort(bson.D{{Key: "timestamp", Value: -1}})
	collection := MongoDB().Collection(collectionName)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	cur, err := collection.Find(ctx, bson.D{}, findOptions)
	if err != nil {
		fmt.Println(" Error", err)
		log.Fatal(err)
	}

	for cur.Next(ctx) {
		var result DevMsg
		err := cur.Decode(&result)
		if err != nil {
			log.Fatal(err)
		} else {
			results = append(results, result)
		}

	}
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	// Close the cursor once finished
	cur.Close(ctx)
	return results, err
}
