package main

import (
	"adhoc.com/mqtt-server/db"
	"adhoc.com/mqtt-server/model"
	"encoding/json"
	"fmt"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/juju/errors"
	"html/template"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

const (
	layoutISO = "2006-01-02"
	layoutUS  = "January 2, 2006"
)

var topic *string
var wg sync.WaitGroup

// DevMsg :	Device Date Set
type DevMsg = model.DevMsg

// PageData : Html Wrapper
type PageData struct {
	Title string
	Data  []DevMsg
}

var messageHandler MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	now := time.Now() // current local time
	var deviceDataMsg DevMsg
	err := json.Unmarshal(msg.Payload(), &deviceDataMsg)
	deviceDataMsg.Raw = string(msg.Payload())
	deviceDataMsg.DeviceIdentifier = strings.Split(msg.Topic(), "/")[2]
	deviceDataMsg.ShowTime = now.Format(time.RFC850)

	if err != nil {
		fmt.Println(err)
		errors.Annotate(err, "Unmarshal Erro")
	} else {
		fmt.Print("MQTT [New Message]: ")
		fmt.Printf("%+v\n", deviceDataMsg)
	}

	res, err := db.Insert(*topic, deviceDataMsg)
	if err != nil {
		errors.Annotate(err, "Mongo [Insert Erro]: ")
		errors.Trace(err)
	} else {
		fmt.Print("Mongo [Insert Okay]: ")
		fmt.Println(res)
	}
}

func runMqttHandler(broker *string, topic *string) {
	username := string(os.Getenv("MQTT_USERNAME"))
	if len(username) == 0 {
		username = "adhoc_user"
	}
	password := string(os.Getenv("MQTT_PASSWORD"))
	if len(password) == 0 {
		password = "adhoc_password"
	}
	opts := MQTT.NewClientOptions().AddBroker(*broker)
	opts.SetClientID("adhoc_user")
	opts.SetDefaultPublishHandler(messageHandler)
	opts.SetUsername(username)
	opts.SetPassword(password)

	opts.OnConnect = func(c MQTT.Client) {
		if token := c.Subscribe(*topic, 0, messageHandler); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}
	client := MQTT.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	} else {
		fmt.Println("Connected successfully to Mqtt")

	}
	wg.Done()
}
func handler(w http.ResponseWriter, r *http.Request) {
	//w.Header().Set("Content-Type", "application/json")

	results, err := db.GetAllCollection(*topic)
	if err != nil {
		fmt.Println("GET MONGO DATA ERROR")
		panic(err)
	}

	data := PageData{
		Title: "My Super Awesome Page of Structs",
		Data:  results,
	}

	// Template
	t, err := template.ParseFiles("index.html")
	if err != nil {
		fmt.Println("Index Error: ")
		fmt.Println(err)
	}
	t.Execute(w, data)
}
func runHTTPHandler() {
	time.Sleep(time.Millisecond * 5000)
	http.HandleFunc("/", handler)
	err := http.ListenAndServe(":8081", nil)
	if err != nil {
		fmt.Println("HTTP ERROR 1")
		fmt.Println(err)
	} else {
		fmt.Println("Http Server is running !!!")
	}
	wg.Done()
}
func main() {
	mqttBrokerEnv := string(os.Getenv("MQTT_URL"))
	if len(mqttBrokerEnv) == 0 {
		mqttBrokerEnv = "ssl://demo.ad-hoc.com:8883"
	}
	subTopicEnv := string(os.Getenv("SUB_TOPIC"))
	if len(subTopicEnv) == 0 {
		subTopicEnv = "application/nbiot/+/up"
	}
	mqttBroker := &mqttBrokerEnv
	topic = &subTopicEnv

	wg.Add(1)
	go runMqttHandler(mqttBroker, topic)
	wg.Add(1)
	go runHTTPHandler()
	wg.Wait()
}

func checkEnvVar(envVarName string) (envVarString string, exists bool) {
	envVarString = string(os.Getenv(envVarName))
	exists = len(envVarString) > 0
	return envVarString, exists
}
