package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// DevMsg is Chirpstack Server Message
type DevMsg struct {
	Distance    float64 `json:"distance" bson:"distance"`
	Barometer   float64 `json:"barometer" bson:"barometer"`
	Battery     float64 `json:"battery" bson:"battery"`
	Temperature float64 `json:"temperature" bson:"temperature"`
	Humidity    float64 `json:"humidity" bson:"humidity"`
	Airpressure float64 `json:"airPressure" bson:"airPressure"`
	Fcnt        int     `json:"fcnt" bson:"fcnt"`
	DeviceIdentifier string `bson:"devID"`
	ShowTime string `bson:"showTime"`
	Raw string
	primitive.Timestamp
}

