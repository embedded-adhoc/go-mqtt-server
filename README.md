# go-mqtt-server
go server get data from mqtt broker and save it to mongo db

Running at Local ( running for all os):

+	Require:

	    MongoDB server version: 4.2.6
	    go version go1.14.2

+	Fast Run:

		go run main.go

+	Build Execute file (build again with new os):

		go get -v all

		go build -o build/main main.go 

		./build/main 

Running with Docker:

		docker-compose up

+	Set broker, topic, username and password in docker-composer.yml

+ 	Rebuild after changing code

		./rebuild.sh

Logging at:
		
            localhost:8081/

			

others CMD:
	
	go test : check Go project  
	go build linux: GOOS=linux go build -o build/main cmd/main.go (Linux)
	go build window: GOOS=windows go build -o build/main cmd/main.go (Window)
	
